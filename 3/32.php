<?php

	include "additional/Helper.php";

	class Swaper{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		
		public function getSwapped(){
			$count = count($this->n);
			for($i = 0; $i < $count; $i++){
				$negativePos = $this->getNegativePos($this->n,$i);
				$this->n[$i] = Helper::swapElement($this->n[$i],$negativePos,$i);
			}
			return $this->n;
		}
		
		private function getNegativePos($arr,$i){
			for($j = 0; $j < count($this->n[$i]); $j++){
				if($arr[$i][$j] < 0){
					return $j;
				}
			}
		}
		
	}
	
	//$arr = Helper::getRand(4,4);
	
	$arr = array(
		array(1,-3,17,5),
		array(7,6,4,-17),
		array(-4,16,4,6),
		array(22,33,-11,3)
	);
	Helper::printArr($arr);
	echo "<br>";
	$swaper = new Swaper($arr);
	Helper::printArr($swaper->getSwapped());