<?php

	include "additional/MaxAndMin.php";
	include "../2/additional/DelInsSw.php";
	include "additional/Helper.php";
	
	class Inserter{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function insert(){
			
			for($j = 0; $j < count($this->n); $j++){
				$negativePos = $this->findFirstNegativePos($this->n,$j);
				if($negativePos != false){
					$this->n[$negativePos] = DIS::insertElement($this->n[$negativePos],
					$j+1,$this->findMinInRow($this->n,$j));
				}else{
					$this->n[$negativePos] = DIS::insertElement($this->n[0],
					$j,$this->findMinInRow($this->n,$j));
				}
					
			}
			return $this->n;
			
		}
		
		public function findFirstNegativePos($arr,$j){
			$index = false;
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i][$j] < 0){
					$index = $i;
				}
			}
			return $index;
		}
		
		public function findMinInRow($arr,$j){
			$min = $arr[0][$j];
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i][$j] < $min){
					$min = $arr[$i][$j];
				}
			}
			return $min;
		}
		
		
	}
	
	$arr = array(
		array(18,9,3,1,8),
		array(10,3,7,8,1),
		array(1,20,-6,19,8)
	);
	//$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	$inserter = new Inserter($arr);
	echo"<br>";
	Helper::printArr($inserter->insert());