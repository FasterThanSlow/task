<?php

	include "additional/MaxAndMin.php";
	include "additional/Helper.php";
	
	class Swaper{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function swap(){
			$maxIndex = 0;
			$minIndex = 0;
			
			for($i = 0; $i < count($this->n); $i++){
				$maxIndex = MaxAndMin::findMaxIndex($this->n[$i]);
				$minIndex = MaxAndMin::findMinIndex($this->n[$i]);
				$temp = $this->n[$i][$maxIndex];	
				$this->n[$i][$maxIndex] = $this->n[$i][$minIndex];
				$this->n[$i][$minIndex] = $temp;
			}
			return $this->n;
		}
	}
	
	$arr = Helper::getRand(3,3);
	Helper::printArr($arr);
	$swaper = new Swaper($arr);
	echo"<br>";
	Helper::printArr($swaper->swap());