
<?php

	include "additional/Sorter.php";
	include "additional/Helper.php";
	include "../2/additional/DelInsSw.php";
	
	class Positioner{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
			
		}
		
		public function getPositioned(){
			$count = count($this->n); 
			$numZero = 0;
			
			for($i = 0; $i < $count; $i++){
				if($this->isEmptyCol($this->n,$i)){
					$this->n = Helper::swapColumns($this->n,$i,$count-1);
					$numZero++;
				}
			}
			
			return $this->n;
		}
		
		private function isEmptyCol($arr,$i){
			for($j = 0; $j < count($arr[$i]); $j++){
				if($arr[$i][$j] != 0){
					return false;
				}
			}
			return true;
		}
	}
	
	$arr = array(
		array(0,0,0,0,0),
		array(7,6,4,17,1),
		array(0,0,0,0,0),
		array(22,33,11,3,0)
	);
	
	Helper::printArr($arr);
	echo "<br>";
	$positioner = new Positioner($arr);
	Helper::printArr($positioner->getPositioned());