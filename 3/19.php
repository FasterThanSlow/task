<?php

	
	include "additional/Helper.php";
	
	class Swaper{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function doSwap(){
			$middle = (int)(count($this->n) / 2);
			for($i = 0; $i < $middle; $i++){
				$this->n = $this->swapRows($this->n,$i,$middle + $i);
			}
			return $this->n;
		}
		
		public function swapRows($arr,$oldIndex,$newIndex){
			$count = count($arr);
			
			for($j = 0; $j < $count; $j++){
				$temp = $arr[$j][$newIndex];
				$arr[$j][$newIndex] = $arr[$j][$oldIndex];
				$arr[$j][$oldIndex] = $temp;
			}
			return $arr;
		}
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$swaper = new Swaper($arr);
	Helper::printArr($swaper->doSwap());