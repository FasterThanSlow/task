<?php
	include "../2/additional/DelInsSw.php";
	include "additional/Helper.php";
	include "additional/MaxAndMin.php";
	
	class Deleter{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function deleting(){
			
			for($i = 0; $i < count($this->n); $i++){
				
				$iBegin = MaxAndMin::findMaxIndex($this->n[$i])+1;
				$temp = DIS::cutArray($this->n[$i],$iBegin,count($this->n[$i])-1);
				$iEnd = $iBegin + MaxAndMin::findMaxIndex($temp) - 1;

				
				for($j = $iBegin; $j <= $iEnd; $j++){
					$this->n[$i] = DIS::deleteElement($this->n[$i],$iBegin);
				}
			}
			return $this->n;
		}
	}
	
	$arr = array(
		array(1,9,3,1,8),
		array(10,3,7,8,1),
		array(1,20,6,19,8)
	);
	
	//$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	$deleter = new Deleter($arr);
	echo"<br>";
	Helper::printArr($deleter->deleting());
