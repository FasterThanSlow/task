<?php

	include "../1/functions.php";
	include "additional/Helper.php";
	
	class Deleter{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function deleting(){
			$countRows = 0;
			$result = $this->n;
			$i = 0;
			
			for($i = 0; $i < count($this->n)-1; $i++){
				if($this->isRowSimple($this->n,$i)){
					$this->n = $this->deleteRow($this->n,$i);
				}
			}
			return $this->n;
		}
		
		private function isRowSimple($arr,$j){
			for($i = 0; $i < count($arr); $i++){
				if(!isSimple($arr[$i][$j])){
					return false;
				}
			}
			return true;
		}
		
		public function deleteRow($arr,$index){
			$result = array();
			
			for($i = 0; $i < count($arr); $i++){
				for($j = 0; $j < count($arr[$i]); $j++){
					if($j != $index){
						$result[$i][] = $arr[$i][$j];
					}
				}
			}
			return $result;
			
			/*for($i = 0; $i < count($arr); $i++){
				for($j = $index; $j < count($arr[$i])-1; $j++){
					$arr[$i][$j] = $arr[$i][$j+1];
				}
			}
			for($i = 0; $i < count($arr); $i++){
				unset($arr[$i][count($arr)]);
			}
			
			return $arr;*/
		}
	}
	
	
	$arr = array(
		array(1,9,3,1,8),
		array(7,6,4,17,0),
		array(1,20,-6,19,-8),
		array(5,2,11,3,0)
	);
	
	//$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	$inserter = new Deleter($arr);
	echo"<br>";
	Helper::printArr($inserter->deleting());
	
	