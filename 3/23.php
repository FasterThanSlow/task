<?php

	include "additional/Helper.php";

	class MatrixSorter{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			$count = count($this->n);
			$positiveArr = $this->getPositive($this->n);
			for ($i = count($positiveArr) - 1; $i >= 0; $i--) {
				for ($j = 0; $j <= ($i-1); $j++){
					if ($positiveArr[$j] < $positiveArr[$j+1]) {
						$temp = $positiveArr[$j];
						$positiveArr[$j] = $positiveArr[$j+1];
						$positiveArr[$j+1] = $temp;
						$this->n = Helper::swapColumns($this->n,$j,$j+1);
					}
				}
			}
			return $this->n;
		}
		
		private function getPositive($arr){
			$positiveArr = array();
			$count = count($arr);
			for($i = 0; $i < $count; $i++){
				$positiveArr[] = $this->getCountPositive($this->n,$i);
			}
			return $positiveArr;
		}
		
		private function getCountPositive($arr,$i){
			$count = 0;
			$temp = count($arr[$i]);
			for($j = 0; $j < $temp; $j++){
				if($arr[$i][$j] > 0){
					$count++;
				}
			}
			return $count;
		}
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());