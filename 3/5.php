<?php
	include "additional/Helper.php";
	
	class Sumer{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}	
		
		public function findSumm($p){
			$summ = 0;
			$flag = true;
			$result = array();
			
			
			for($j = 0; $j < count($this->n); $j++){
				for($i = 0; $i < count($this->n); $i++){
					if($this->n[$i][$j] % $p != 0){
						$flag = false;
						break;
					}
				}
				if($flag){
					$result[] = $this->summRow($this->n,$j);
				}
				$flag = true;
			}
			
			return $result;
		}
		
		private function summRow($arr, $j){
			$summ = 0;
			
			for($i = 0; $i < count($arr); $i++){
				$summ += $arr[$i][$j];
			}
			
			return $summ;
		}
	}
	
	//$arr = Helper::getRand(3,3);
	$arr = array(
		array(2,10,7),
		array(32,18,0),
		array(6,4,4)
	);
	
	Helper::printArr($arr);
	$sumer = new Sumer($arr);
	echo "<br>";
	print_r( $sumer->findSumm(2));