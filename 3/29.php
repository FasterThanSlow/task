<?php

	include "additional/Sorter.php";
	include "additional/Helper.php";
	include "../2/additional/DelInsSw.php";
	
	class MatrixSorter{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			$morePos = array();
			$moreNeg = array();
			$negEqualPos = array();
			
			$count = count($this->n);
			
			for($i = 0; $i < $count; $i++){
				switch($this->compareNegAndPos($this->n,$i)){
					case 1:
						$morePos[] = $this->n[$i];
					break;
					case 0:
						$negEqualPos[] = $this->n[$i];
					break;
					case -1:
						$moreNeg[] = $this->n[$i];
					break;
				}
			}
			$this->n = DIS::concat($morePos,$negEqualPos);
			$this->n = DIS::concat($this->n,$moreNeg);
			return $this->n;
		}
		
		private function compareNegAndPos($arr,$i){
			$countNeg = 0;
			$countPos = 0;
			$count = count($arr);
			
			for($j = 0; $j < $count; $j++){
				if($arr[$i][$j] > 0){
					$countPos++;
				}
				else{
					$countNeg++;
				}
			}
			
			if($countPos > $countNeg){
				return 1;
			}
			elseif($countPos == $countNeg){
				return 0;
			}
			else{
				return -1;
			}
		}
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());