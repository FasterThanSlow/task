<?php
	
	include "additional/Helper.php";
	
	class Sumer{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function findSummMain(){
			$summ = 0;
			
			for($i = 0; $i < count($this->n); $i++){
				$summ += $this->n[$i][$i];
			}
			
			return $summ;
		}
		
		
		public function findSummMainPlus(){
			$summ = 0;
			
			for($i = 0; $i < count($this->n) - 1 ; $i++){
				for($j = $i; $j < count($this->n) - 1; $j++){
					$summ += $this->n[$i][$i+1];
				}
			}
			
			return $summ;
		}
		
		public function findSummMainMinus(){
			$summ = 0;
			
			for($i = 1; $i < count($this->n); $i++){
				$summ += $this->n[$i][$i-1];
			}
			
			return $summ;
		}
		
	}
	
	$arr = Helper::getRand(3,3);
	Helper::printArr($arr);
	
	$summ = new Sumer($arr);
	$result = array($summ->findSummMain(),$summ->findSummMainPlus(),$summ->findSummMainMinus());
	echo "<br>";
	print_r($result);
	