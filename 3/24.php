<?php

	include "additional/Sorter.php";
	include "additional/Helper.php";
	include "../2/additional/DelInsSw.php";
	
	class Positioner{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
			
		}
			
		public function getPositioned(){
			$count = count($this->n);
			
			for($i = 0; $i < $count; $i++){
				$numWithoutZero = $this->cutNumWithout($this->n[$i],0);
				$numWithoutZero = Sorter::sortD($numWithoutZero);
				$numZero = $this->cutNum($this->n[$i],0);
				$this->n[$i] = DIS::concat($numZero,$numWithoutZero);
				
			}
			return $this->n;
		}

		private function cutNumWithout($arr,$n){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] != $n){
					$result[] = $arr[$i];
					
				}
			}
			return $result;
		}
		
		private function cutNum($arr,$n){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] == $n){
					$result[] = $n;
					
				}
			}
			return $result;
		}
	}
	$arr = array(
		array(1,9,0,0,8),
		array(7,6,4,17,0),
		array(1,20,0,19,-8),
		array(0,0,11,3,0)
	);
	
	//$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$positioner = new Positioner($arr);
	Helper::printArr($positioner->getPositioned());