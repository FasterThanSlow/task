<?php

	include "additional/Helper.php";
	include "additional/MaxAndMin.php";
	include "additional/Sorter.php";
	include "../2/additional/DelInsSw.php";

	class MatrixSorter{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted($p){
			$count = count($this->n);
			$arrWithoutDivP = array();
			$arrDivP = array();
			
			
			for($i = 0; $i < $count; $i++){
				$arrDivP = $this->cutNumDivP($this->n[$i],$p);
				$arrWithoutDivP = $this->cutNumWithoutDivP($this->n[$i],$p);
				$arrDivP = Sorter::sortU($arrDivP);
				$this->n[$i] = DIS::concat($arrDivP,$arrWithoutDivP);
			}
			return $this->n;
		}
		
		private function cutNumWithoutDivP($arr,$p){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] % $p != 0){
					$result[] = $arr[$i];
					
				}
			}
			return $result;
		}
		
		private function cutNumDivP($arr,$p){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] % $p == 0){
					$result[] = $arr[$i];
					
				}
			}
			return $result;
		}
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted(2));