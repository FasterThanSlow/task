<?php 
	include "additional/Helper.php";
	
	class Sumer{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function findSumm(){
			$summ = 0;
			$nums = array(0,1,3,8);
			
			for($i = 0; $i < count($this->n); $i++){
				for($j = 0; $j < count($this->n[$i]); $j++){
					if($this->isOnlyNums($this->n[$i][$j],$nums)){
						$summ += $this->n[$i][$j];
					}
				}
			}
			
			return $summ;
		}
		
		private function isOnlyNums($n,$nums){
			$count1 = 0;
			$count2 = 0;
			
			while($n != 0){
				$k = $n % 10;				
				for($i = 0; $i < count($nums); $i++){
					if($k == $nums[$i]){
						$count2++;
						break;
					}
				}
	
				$count1++;
			
				$n /= 10;	
			}
			
			if($count1 != $count2)
				return false;
			else 
				return true;
		}
		
	}
	
	//$arr = Helper::getRand(3,3);
	$arr = array(
		array(1,3,7),
		array(31,17,0),
		array(-6,5,4)
	);
	Helper::printArr($arr);
	$sumer = new Sumer($arr);
	echo "<br>";
	echo $sumer->findSumm();