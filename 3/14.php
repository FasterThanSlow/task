<?php

include "additional/Helper.php";

class Deleter{
	public $n;
	
	public function __construct($n){
		$this->n = $n;
	}
	
	public function deleting(){
		for($i = 0; $i < count($this->n); $i++){
			if($this->isEmptyCol($this->n,$i)){
				$this->n = $this->deleteCol($this->n,$i);
			}
		}
		return $this->n;
	}
	
	private function deleteCol($arr,$i){
		$result = $arr;
		
		for($j = $i; $j < count($arr)-1; $j++){
			for($k = 0; $k < count($arr[$j]); $k++){
				$result[$j][$k] = $result[$j+1][$k];
			}
		}
		unset($result[count($result)-1]);
		return $result;
	}
	
	private function isEmptyCol($arr,$i){
		for($j = 0; $j < count($arr[$i]); $j++){
			if($arr[$i][$j] != 0){
				return false;
			}
		}
		return true;
	}
}

	
	$arr = array(
		array(18,9,3,1,8),
		array(0,0,0,0,0),
		array(1,20,-6,19,-8),
		array(0,0,0,0,0)
	);
	
	//$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	$inserter = new Deleter($arr);
	echo"<br>";
	//Helper::printArr($inserter->deleting());