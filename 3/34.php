<?php

	include "additional/Sorter.php";
	include "additional/Helper.php";
	include "../2/additional/DelInsSw.php";
	
	class MatrixSorter{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			$indexMax = $this->findMaxColumnSumm($this->n);
			$this->n = Helper::swapColumns($this->n,$indexMax,0);
			
			for ($j = count($this->n[0]) - 1; $j >= 0; $j--) {
				for ($k = 1; $k <= ($j-1); $k++){
					if ($this->n[$k][0] > $this->n[$k+1][0]) {
						$this->n = Helper::swapColumns($this->n,$k,$k+1);
					}
				}
			}
			return $this->n;
		}
		
		private function findMaxColumnSumm($arr){
			$max = $this->findColmnSum($arr,0);
			$index = 0;
			for($i = 1; $i < count($arr); $i++){
				$current = $this->findColmnSum($arr,$i);
				if($max < $current){
					$max = $current;
					$index = $i;
				}
			}
			return $index;
		}
		
		private function findColmnSum($arr,$i){
			$count = count($arr[$i]);
			$summ = 0;
			
			for($j = 0; $j < $count; $j++){
				$summ += $arr[$i][$j];
			}
			
			return $summ;
		}
		
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());