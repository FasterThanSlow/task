<?php
	include "additional/Helper.php";
	class Finder{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function findMaxUppOrDown(){
			$max = array();
			$l = 0;
			
			for($i = 0; $i < count($this->n); $i++){
				if($this->isUpper($this->n,$i) ||	 $this->isDown($this->n,$i)){
					$max[$i] = $this->findMax($this->n, $i);
				}
			}
			return $max;
		}
		
		function findMinUppOrDown(){
			$min = array();
			$l = 0;
			
			for($i = 0; $i < count($this->n); $i++){
				if($this->isUpper($this->n,$i) || $this->isDown($this->n,$i)){
					$min[$i] = $this->findMin($this->n, $i);
				}
			}
			return $min;
		}
		
		private function findMin($arr,$i){
			$min = $arr[$i][0];
			for($j = 1; $j < count($arr[$i]); $j++){
				if($arr[$i][$j] < $min){
					$min = $arr[$i][$j];
				}
			}
			return $min;
		}
		
		
		private function findMax($arr,$i){
			$max = $arr[$i][0];
			for($j = 1; $j < count($arr[$i]); $j++){
				if($arr[$i][$j] > $max){
					$max = $arr[$i][$j];
				}
			}
			return $max;
		}
		
		private function isUpper($arr,$i){
			$flag = true;
			
			for($j = 0; $j < count($arr[$i])-1; $j++){
				if($arr[$i][$j] > $arr[$i][$j+1]){
					$flag = false;
					break;
				}
			}
			
			return $flag;
		}
		
		private function isDown($arr,$i){
			$flag = true;
			
			for($j = 0; $j < count($arr[$i])-1; $j++){
				if($arr[$i][$j] < $arr[$i][$j+1]){
					$flag = false;
					break;
				}
			}
			
			return $flag;
		}
	}
	
	$res = array(
		array(5,4,3,2),
		array(2,7,1,5),
		array(4,6,9,12)
	);
	$finder = new Finder($res);
	Helper::printArr($res);
	echo "<br> Max: ";
	print_r($finder->findMaxUppOrDown()); 
	echo "<br> Min: ";
	print_r($finder->findMinUppOrDown()); 