<?php
	
	include "additional/Helper.php";
	
	class Swaper{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function swap(){
			$maxTopPosition = $this->findMaxTopPos($this->n); 
			$maxBottomPosition = $this->findMaxBottomPos($this->n);

			$temp = $this->n[$maxTopPosition[0]][$maxTopPosition[1]];
			$this->n[$maxTopPosition[0]][$maxTopPosition[1]] = $this->n[$maxBottomPosition[0]][$maxBottomPosition[1]];
			$this->n[$maxBottomPosition[0]][$maxBottomPosition[1]] = $temp;
			return $this->n;
		}
		
		private function findMaxTopPos($arr){
			$max = $arr[1][0];
			$result = array(1,0);
			
			for($i = 2; $i < count($arr); $i++){
				for($j = 0; $j < $i; $j++){
					if($arr[$i][$j] > $max){
						$max = $arr[$i][$j];
						$result[0] = $i;
						$result[1] = $j;
					}
				}
			}
			return $result;
		}
		
		private function findMaxBottomPos($arr){
			$max = $arr[0][1];
			$result = array(0,1);
			for($i = 0; $i < count($arr); $i++){
				for($j = $i+1; $j < count($arr[$i]); $j++){
					if($arr[$i][$j] > $max){
						$max = $arr[$i][$j];
						$result[0] = $i;
						$result[1] = $j;
					}
				}
			}
			return $result;
		}
	}
	
	
	$arr = Helper::getRand(3,3);
	Helper::printArr($arr);
	$swaper = new Swaper($arr);
	echo"<br>";
	Helper::printArr($swaper->swap($arr));