<?php

	include "additional/Helper.php";
	
	class Inserter{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function start($arr){
			$flag = false;
			for($i = 0; $i < count($this->n); $i++){
				if($this->issetZero($this->n,$i)){
					$this->n = $this->insert($this->n,$arr,$i+1);
					$flag = true;
				}
				
			}
			if(!$flag){
				$this->n = $this->insert($this->n,$arr,count($this->n));
			}
			return $this->n;
		}
		
		private function insert($arr,$arr2,$index){
			for($i = 0; $i < count($arr); $i++){
				for($j = count($arr[$i]); $j > $index; $j--){
					$arr[$i][$j] = $arr[$i][$j-1];
				}
			}
			
			for($i = 0; $i < count($arr); $i++){
				$arr[$i][$index] = $arr2[$i];
			}
			
			return $arr;
		}
		
		private function issetZero($arr, $j){
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i][$j] == 0){
					return true;
				} 
			}
			return false;
		}
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$inserter = new Inserter($arr);
	Helper::printArr($inserter->start(array(2,1,9,3)));