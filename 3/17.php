<?php
	
	include "additional/Helper.php";
	
	class Swaper{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function doSwap(){
			$dChangeNums = 0;
			$count = count($this->n);
			$index = 0;
			
			for($i = 0; $i < $count; $i++){
				$temp = $this->getCountDigitChange($this->n,$i);
				if($temp > $dChangeNums){
					$dChangeNums = $temp;
					$index = $i;
				}
			}
			
			$this->n =	Helper::swapColumns($this->n,$index,0);
			return $this->n;
		}
		
		
		private function getCountDigitChange($arr,$i){
			$count = 0;
			$num = count($arr[$i]);
			for($j = 0; $j < $num-1; $j++){
				if($arr[$i][$j] >= 0 && $arr[$i][$j+1] < 0 || $arr[$i][$j] < 0 && $arr[$i][$j+1] >= 0){
					$count++;
				}
			}
			return $count;
		}
		
		
		
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$swaper = new Swaper($arr);
	Helper::printArr($swaper->doSwap());
	
	