<?php

	include "additional/Sorter.php";
	include "additional/Helper.php";
	
	class MatrixSorter{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			$count = count($this->n);
			for($i = 0; $i < $count; $i++){
				if($this->isFirstNegative($this->n,$i)){
					$this->n[$i] = Sorter::sortU($this->n[$i]);
				}
			}
			return $this->n;
		}
		
		private function isFirstNegative($arr,$i){
			if($arr[$i][0] < 0){ 
				return true;
			}
			else{
				return false;
			}
		}
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());