<?php

	include "additional/Helper.php";
	include "additional/MaxAndMin.php";
	include "../2/additional/DelInsSw.php";
	
	class Deleter{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function deleting(){
			for($i = 0; $i < count($this->n);$i++){
				$maxIndex = MaxAndMin::findMaxIndex($this->n[$i]);
				$this->n[$i] = DIS::deleteElement($this->n[$i],$maxIndex);
			}
			return $this->n;
		}
	}
	
	$arr = Helper::getRand(3,3);
	Helper::printArr($arr);
	$deleter = new Deleter($arr);
	echo"<br>";
	Helper::printArr($deleter->deleting());