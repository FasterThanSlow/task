<?php

	include "additional/Helper.php";

	class MatrixSorter{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			for ($j = count($this->n[0]) - 1; $j >= 0; $j--) {
				for ($k = 0; $k <= ($j-1); $k++){
					if ($this->n[0][$k] > $this->n[0][$k+1]) {
						$this->n = Helper::swapRows($this->n,$k,$k+1);
					}
				}
			}
			return $this->n;
		}
		
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());