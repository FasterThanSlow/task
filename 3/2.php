<?php

	include "additional/Helper.php";
	
	class Sumer{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function getMaxSumms(){
			$summ = $this->getSumms($this->n,0);
			
			for($i = 1; $i < count($this->n); $i++){
				$temp = $this->getSumms($this->n,$i);
				if($temp < $summ){
					$summ = $temp;
				}
			}
			return $summ;
		}
		
		private function getSumms($arr,$i){
			$summ = 0;
			$count = 0;
			
			for($j = 0; $j < count($arr[$i]); $j++){
				if($arr[$i][$j] > 0){
					$count++;
					$summ += $arr[$i][$j];
				}
			}
			if($count > 0)
				$summ /= $count;
			return $summ;
		}
	}
	
	
	$arr = Helper::getRand(3,3);
	Helper::printArr($arr);
	
	$summ = new Sumer($arr);
	$result = $summ->getMaxSumms();
	echo $result;