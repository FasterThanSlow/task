<?php
	
	include "additional/Helper.php";
	include "additional/MaxAndMin.php";

	class Swaper{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function swap(){
			$min = $this->findMin($this->n);
			$max = $this->findMax($this->n);
			$this->n = $this->swapRows($this->n,$min[2],$max[2]);
			return $this->n;
		}
		
		private function findMax($arr){
			$max = array($arr[0][0],0,0);
			for($i = 0; $i < count($arr); $i++){
				for($j = 0; $j < count($arr[$i]); $j++){
					if($arr[$i][$j] > $max[0]){
						$max[0] = $arr[$i][$j];
						$max[1] = $i;
						$max[2] = $j;
					}
				}
			}
			return $max;
		}
		
		private function findMin($arr){
			$min = array($arr[0][0],0,0);
			for($i = 0; $i < count($arr); $i++){
				for($j = 0; $j < count($arr[$i]); $j++){
					if($arr[$i][$j] < $min[0]){
						$min[0] = $arr[$i][$j];
						$min[1] = $i;
						$min[2] = $j;
					}
				}
			}
			return $min;
		}
		
		private function swapRows($arr,$col1,$col2){
			for($i = 0; $i < count($arr); $i++){
				$temp = $arr[$i][$col1];
				$arr[$i][$col1] = $arr[$i][$col2];
				$arr[$i][$col2] = $temp;
			}
			return $arr;
		}
		
	}
	
	$arr = array(
		array(18,9,3,1,8),
		array(10,3,7,8,1),
		array(1,20,-6,19,-8)
	);
	//$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	$inserter = new Swaper($arr);
	echo"<br>";
	Helper::printArr($inserter->swap());