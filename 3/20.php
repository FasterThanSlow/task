<?php
	
	include "additional/Helper.php";
	
	class Finder{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getCountsMax(){
			$count = count($this->n);
			$result = array();
			
			for($i = 0; $i < $count; $i++){
				$result[] = $this->getMaxOneTime($this->n,$i);
			}
			
			return $result;
		}
	
		public function getMaxOneTime($arr,$i){
			$count = count($arr[$i]);
			$max = $arr[$i][0];
			for($j = 1; $j < $count; $j++){
				if($this->isUnique($arr[$i],$arr[$i][$j])){
					if($max < $arr[$i][$j]){
						$max = $arr[$i][$j];
					}
				}
			}
			return $max;
		}
	
		public function isUnique($arr,$num){
			$count = count($arr);
			$numsCount = 0;
			
			for($i = 0; $i < $count; $i++){
				if($num == $arr[$i]){
					$numsCount++;
				}
			}
			
			if($numsCount == 1){
				return true;
			} 
			else{
				return false;
			}
		}
	}
	
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$finder = new Finder($arr);
	
	
	echo "<pre>".print_r($finder->getCountsMax(),true)."</pre>";