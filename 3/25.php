<?php

	include "additional/Helper.php";
	include "../1/functions.php";

	class MatrixSorter{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			$count = count($this->n);
			$simpleCount = $this->getSimples($this->n);
			for ($i = count($simpleCount) - 1; $i >= 0; $i--) {
				for ($j = 0; $j <= ($i-1); $j++){
					if ($simpleCount[$j] > $simpleCount[$j+1]) {
						$temp = $simpleCount[$j];
						$simpleCount[$j] = $simpleCount[$j+1];
						$simpleCount[$j+1] = $temp;
						$this->n = Helper::swapColumns($this->n,$j,$j+1);
					}
				}
			}
			return $this->n;
		}
		
		private function getSimples($arr){
			$positiveArr = array();
			$count = count($arr);
			for($i = 0; $i < $count; $i++){
				$positiveArr[] = $this->getCountSimple($this->n,$i);
			}
			return $positiveArr;
		}
		
		private function getCountSimple($arr,$i){
			$count = 0;
			$temp = count($arr[$i]);
			for($j = 0; $j < $temp; $j++){
				if(isSimple($arr[$i][$j])){
					$count++;
				}
			}
			return $count;
		}
	}
	/*$arr = array(
		array(1,9,18,3,8),
		array(7,6,4,17,1),
		array(11,20,81,19,-8),
		array(22,33,11,3,0)
	);*/
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());