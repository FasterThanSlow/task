<?php

	include "../1/functions.php";
	include "additional/Helper.php";
	
	class Changer{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function change(){
			for($i=0;$i < count($this->n[0]); $i++){
				if($this->isPositiveRow($this->n,$i)){
					$this->n = $this->changeRowsElToSumm($this->n,$i);
				}
			}
			return $this->n;
		}
		
		private function isPositiveRow($n,$j){
			for($i = 0; $i < count($n); $i++){
				if($n[$i][$j] > 0){
					return false;
				}
			}
			return true;
		}
		
		private function changeRowsElToSumm($arr,$i){
			for($j = 0; $j < count($arr[$i]); $j++){
				$arr[$j][$i] = summNumber($arr[$j][$i]);
			}
			return $arr;
		}
	}
	
	$arr = Helper::getRand(3,3);
	Helper::printArr($arr);
	$swaper = new Changer($arr);
	echo"<br>";
	Helper::printArr($swaper->change());