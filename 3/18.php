<?php

	include "additional/Helper.php";
	
	class Swaper{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function doSwap(){
			$count = count($this->n[0]);
			$minOddCount = $this->getCountOdd($this->n,0);
			$index = 0;
			
			for($j = 0; $j < $count; $j++){
				$temp = $this->getCountOdd($this->n,$j);
				if($temp < $minOddCount){
					$minOddCount = $temp;
					$index = $j;
				}
			}
			
			$this->n = $this->swapRows($this->n,$index,count($this->n[0])-1);
			return $this->n;
			
		}
		
		public function swapRows($arr,$oldIndex,$newIndex){
			$count = count($arr);
			
			for($j = 0; $j < $count; $j++){
				$temp = $arr[$j][$newIndex];
				$arr[$j][$newIndex] = $arr[$j][$oldIndex];
				$arr[$j][$oldIndex] = $temp;
			}
			return $arr;
		}
		
		private function getCountOdd($arr,$j){
			$count = 0;
			$num = count($arr);
			for($i = 0; $i < $num; $i++){
				if($arr[$i][$j] % 2 != 0){
					$count++;
				}
			}
			return $count;
		}
		
		
		
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$swaper = new Swaper($arr);
	Helper::printArr($swaper->doSwap());
	
	