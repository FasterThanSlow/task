<?php

	include "additional/Helper.php";
	include "additional/MaxAndMin.php";

	class MatrixSorter{
		
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted(){
			$maxArr = $this->getAllRowsMax($this->n);
			$count = count($maxArr);
			
			for ($j = $count - 1; $j >= 0; $j--) {
				for ($k = 0; $k <= ($j-1); $k++){
					if ($maxArr[$k] < $maxArr[$k+1]) {
						$temp = $maxArr[$k];
						$maxArr[$k] = $maxArr[$k+1];
						$maxArr[$k+1] = $temp;
						$this->n = Helper::swapRows($this->n,$k,$k+1);
					}
				}
			}
			return $this->n;
		}
		
		private function getRowMax($arr,$j){
			$count = count($arr);
			$temp = array();
			
			for($i = 0; $i < $count; $i++){
				$temp[] = $arr[$i][$j];
			}
			
			$max = MaxAndMin::findMaxValue($temp);
			return $max;
		}
		
		public function getAllRowsMax($arr){
			$count = count($arr);
			$result = array();
			
			for($j = 0; $j < $count; $j++){
				$result[] = $this->getRowMax($arr,$j);
			}
			
			return $result;
			
		}
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted());