<?php

	include "additional/Sorter.php";
	include "additional/Helper.php";
	include "../2/additional/DelInsSw.php";
	
	class MatrixSorter{
		public $n;
		
		public function __construct($n){
			if(Helper::isMatrix($n)){
				$this->n = $n;
			}
		}
		
		public function getSorted($p){
			$flag = false;
			$this->n = $this->sortD($this->n);
			$count = count($this->n);
			
			
			for($i = 0; $i < $count; $i++){
				$counti = count($this->n[$i]);
				for($j = 0; $j < $counti; $j++){
					if($this->n[$i][$j] < $p){
						$flag = true;
						$this->n[$i] = DIS::insertElement($this->n[$i],$j,$p); 
						break;
					}
					
				}
				if(!$flag){
					$this->n[$i] = DIS::insertElement($this->n[$i],count($this->n[$i]),$p);
				}
				$flag = false;
			}
			
			return $this->n;
		}
		
		private function sortD($arr){
			$count = count($arr);
			for($i = 0; $i < $count; $i++){
				$arr[$i] = Sorter::sortD($arr[$i]);
			}
			return $arr;
		}
		
	}
	
	$arr = Helper::getRand(4,4);
	Helper::printArr($arr);
	echo "<br>";
	$sorter = new MatrixSorter($arr);
	Helper::printArr($sorter->getSorted(2));