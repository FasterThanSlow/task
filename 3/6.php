<?php

	include "additional/Helper.php";
	
	class Multipler{
		public $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function multiPositive($m){
			for($i = 0; $i < count($this->n); $i++){
				if($this->isPositive($this->n,$i)){
					$result = $this->multiple($m,$this->n[$i]);
					return $result;
				}
			}
		}
		
		private function multiple($n,$m){
			$result = array(array());
			
			for($i = 0; $i < count($n); $i++){
				for($j = 0; $j < count($n[$i]); $j++){
					$result[$i][$j] = $n[$i][$j] * $m[$j];
				}
			}
			
			return $result;
		}
		
		private function isPositive($n,$i){
			for($j = 0; $j < count($n[$i]); $j++){
				if($n[$i][$j] < 0){
					return false;
				}
			}
			return true;
		}
	}
	
	//$arr = Helper::getRand(3,3);
	$arr = array(
		array(2,10,7),
		array(32,18,0),
		array(6,4,4)
	);
	Helper::printArr($arr);
	$sumer = new Multipler($arr);
	echo "<br>";
	$m = array(array(1,2,5),array(3,2,4),array(1,1,1));
	Helper::printArr($m);
	echo "<br>";
	Helper::printArr($sumer->multiPositive($m));