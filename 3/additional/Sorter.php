<?php

	class Sorter{
		
		public static function sortU($arr){
			$result = $arr;
			for ($i = count($result) - 1; $i >= 0; $i--) {
				for ($j = 0; $j <= ($i-1); $j++){
					if ($result[$j] > $result[$j+1]) {
						$temp = $result[$j];
						$result[$j] = $result[$j+1];
						$result[$j+1] = $temp;
					}
				}
			}
			return $result;
        }

		
		public static function sortD($arr){
			$result = $arr;
			for ($i = count($result) - 1; $i >= 0; $i--) {
				for ($j = 0; $j <= ($i-1); $j++){
					if ($result[$j] < $result[$j+1]) {
						$temp = $result[$j];
						$result[$j] = $result[$j+1];
						$result[$j+1] = $temp;
					}
				}
			}
			return $result;
		}
	}