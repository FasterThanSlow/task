<?php

	class MaxAndMin{
		public static function findMaxValue($arr){
			$max = $arr[0];
			for($i=0; $i < count($arr); $i++){
				if($arr[$i] > $max){
					$max = $arr[$i];
				}
			}
			return $max;
		}
		
		
		public static function findMinValue($arr){
			$min = $arr[0];
			for($i=0; $i < count($arr); $i++){
				if($arr[$i] < $min){
					$min = $arr[$i];
				}
			}
			return $min;
		}
		
		public static function findMaxIndex($arr){
			$index = 0;
			$max = $arr[0];
			for($i=0; $i < count($arr); $i++){
				if($arr[$i] > $max){
					$max = $arr[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
		public static function findMinIndex($arr){
			$min = $arr[0];
			$index = 0;
			for($i=0; $i < count($arr); $i++){
				if($arr[$i] < $min){
					$min = $arr[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
	}