<?php

	class Helper{
		
		public static function getRand($n,$m){
			
			$result = array(array());
			for($i = 0; $i < $n; $i++){
				for($j = 0; $j < $m; $j++){
					$result[$i][] = rand(-50,100);
				}
			}
			return $result;
		}
		
		public static function printArr($arr){
			if(self::isMatrix($arr)){
				echo "<table>";
				for($i = 0; $i < count($arr); $i++){
					echo "<tr>";
					for($j = 0; $j < count($arr[$i]); $j++){
						echo "<td>".$arr[$i][$j]."</td>";
					}
					echo "</tr>";
				}
				echo "</table>";
			}
			else{
				return false;
			}
		}
		
		public static function isMatrix($arr){
			if(is_array($arr)){
				for($i = 0; $i < count($arr); $i++){
					if(!is_array($arr[$i])){
						return false;
					}
				}
			}
			else{
				return false;
			}
			return true;
		}
		
		public static function swapRows($arr,$oldIndex,$newIndex){
			if(self::isMatrix($arr)){
				$count = count($arr);
				
				for($j = 0; $j < $count; $j++){
					$temp = $arr[$j][$newIndex];
					$arr[$j][$newIndex] = $arr[$j][$oldIndex];
					$arr[$j][$oldIndex] = $temp;
				}
				return $arr;
			}
			else{
				return false;
			}
		}
		
		
		public static function swapColumns($arr,$oldIndex,$newIndex){
			$count = count($arr[$oldIndex]);
			for($j = 0; $j < $count; $j++){
				$temp = $arr[$newIndex][$j];
				$arr[$newIndex][$j] = $arr[$oldIndex][$j];
				$arr[$oldIndex][$j] = $temp;
			}
			return $arr;
		}
		
		public static function swapElement($arr,$oldIndex,$newIndex){
			$temp = $arr[$oldIndex];
			$arr[$oldIndex] = $arr[$newIndex];
			$arr[$newIndex] = $temp;
			return $arr;
		}
	}