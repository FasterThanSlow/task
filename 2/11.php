<?php

	include "functions.php";
	
	
	class MaxIMin{
		
		protected $n;
		
		public function __construct($n){
			if(is_array($n))
				$this->n = $n;
		}
		
		public function findMaxIndex(){
			$max = $this->n[0];
			$index = 0;
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] > $max){
					$max = $this->n[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
		public function findMaxValue(){
			$max = $this->n[0];
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] > $max){
					$max = $this->n[$i];
				}
			}
			return $max;
		}
		
		public function findMinIndex(){
			$min = $this->n[0];
			$index = 0;
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] < $min){
					$min  = $this->n[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
		public function findMinValue(){
			$min  = $this->n[0];
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] < $min){
					$min  = $this->n[$i];
				}
			}
			return $min ;
		}
		
	}
	
	class Searcher extends MaxIMin{
		
		public function __construct($n){
			parent::__construct($n);
		}
		
		public function searchFBifMax(){
			$maxIndex = $this->findMaxIndex();
			for($i = 0; $i < $maxIndex; $i++){
				if($this->n[$i] < 0){
					return $this->n[$i];
				}
			}
		}
		
		public function searchLAftMin(){
			$minIndex = $this->findMinIndex();
			$positive = 0;
			for($i = $minIndex; $i < count($this->n); $i++){
				if($this->n[$i] > 0){
					$positive = $this->n[$i];
				}
			}
			return $positive;
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new Searcher($arr);
	echo  $finder->searchFBifMax()."<br>";
	echo  $finder->searchLAftMin()."<br>";