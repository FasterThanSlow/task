<?php
	include "additional/DelInsSw.php";
	include "additional/MaxAndMin.php";
	
	class Deleter{
		
		public static function deleting($arr){
			$result = $arr;
			$j = 0;
			
			$minIndex = MaxAndMin::findMinIndex($result);
			$temp = DIS::cutArray($result,$minIndex,count($result)-1);
			$maxIndex = MaxAndMin::findMaxIndex($temp)+$minIndex;
			DIS::printArr($temp);
			for($i = $minIndex + 1; $i < $maxIndex; $i++){
				$result = DIS::deleteElement($result,$i-$j);
				$j++;
			}
			echo $minIndex." ";
			echo $maxIndex." ";	
			return $result;
		} 
	}
	
	$arr = array(1,10,-1,8,9,4,6,7,7,1,-1,2);
	//$arr = DIS::getRandArr(10);
	DIS::printArr($arr);
	DIS::printArr(Deleter::deleting($arr));