<?php


	include "additional/MathClass.php";
	include "additional/DelInsSw.php";
	
	class Polynom{
		
		public static function find($arr,$x){
			$result = 0;
			$xPower = 1;
			
			for($i = 0; $i < count($arr); $i++){
				$result += $arr[$i]*$xPower;
				$xPower *= $x;
			}
			
			return $result;
		}
		
	public static function findAllDiff($arr,$x){
			$temp = $arr;
			$result = array();
			for($i = 1; $i < count($arr) - 1; $i++){
				for($j = 0; $j < count($arr); $j++){
					$temp[$j] = $temp[$j]*$j*MathClass::power($x,$j-1);
				}
				$result[$i] = DIS::getSumm($temp);
			}
					
			return $result;
		}
		
		
	}
	
	$arr = array(2,4,1,1);
	
	print_r($arr);
	echo Polynom::find($arr,2)." ";

	print_r(Polynom::findAllDiff($arr,2));	