<?php

include "functions.php";

class SearcherSequences{
	private $n;
	
	public function __construct($n){
		$this->n = $n;
	}
	
	public function Search($number){
		$summ = 0;
		for($i = 1; $i < count($this->n);$i++){
			for($j = 0; $j < count($this->n) - $i; $j++){
				for($k = $j; $k < $j+$i; $k++){
					$summ += $this->n[$k];
				}
				
				if($summ == $number){
					return true;
				}
				$summ = 0;
			}
		}
		return false;
	}
	
}

	$arr = array(1,4,5,9,13);
	$searcher = new SearcherSequences($arr);
	if($searcher->search(19)){
		echo "В массиве есть последовательность = 19";
	}else
		echo "В массиве нет последовательности = 19";