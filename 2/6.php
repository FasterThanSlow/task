<?php

	include "functions.php";
	
	function changeToIndex($n){
		if(is_array($n)){
			$srArifm = 0;
			$max = array();
			
			for($i = 0; $i < count($n); $i++){
				$srArifm += $n[$i];
			}
			$srArifm /= count($n) - 1;
			
			for($i = 0; $i < count($n); $i++){
				if($n[$i] > $srArifm){
					if($n[$i] % 2 == 0){
						$max[] = $n[$i];
					}
				}
			}
			
			return $max;
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	echo "<br>";
	printArr(changeToIndex($arr));