<?php
	include "functions.php";
	class Inserter{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function insert($k){
			$result = $this->n;
			for($i = 0; $i < $k; $i++){
				$result = $this->insertAfter($result,1,rand(1,30));
			}
			$result = self::sorting($result);
			return $result;
		}
		
		public static function insertAfter($arr, $index, $element){
			$result = $arr;
			$result[] = $result[count($result)-1];
			for($i = count($result)-1; $i > $index; $i--){
				$result[$i] = $result[$i-1];
			}
			$result[$index+1] = $element;
			return $result;
		}
		
		public static function sorting($arr){
			$size = count($arr)-1;
			$result = $arr;
			for ($i = $size; $i>=0; $i--) {
				for ($j = 0; $j<=($i-1); $j++)
						if ($result[$j]>$result[$j+1]) {
							$k = $result[$j];
							$result[$j] = $result[$j+1];
							$result[$j+1] = $k;
						}
			}
			return $result;
			
		}
	}
	
	//$arr = array(1,8,-7,-6,12,-16,19,90,-150);
	$arr = randArr(10);
	printArr($arr);
	//$arr = array(1,0,0,0,0,9,0,0,0,0,0,0,3,5,4);
	$searcher = new Inserter($arr);
	printArr($searcher->insert(3));