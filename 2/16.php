<?php

class SearcherSequences{
	private $n;
	
	public function __construct($n){
		$this->n = $n;
	}
	
	public function search(){
		$count = 1;
		$maxCount = 0;
		
		for($i = 0; $i < count($this->n)-1; $i++){
			if($this->n[$i] == $this->n[$i+1]){
				$count++;
			}else
				$count=1;
			
			if($maxCount < $count){
				$maxCount = $count;
			}
		}
		return $maxCount;
	}
}

$arr = array(1,2,9,9,10,10,10,3,5,4);
$searcher = new SearcherSequences($arr);
echo $searcher->search();