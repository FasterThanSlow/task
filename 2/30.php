<?php

	include "additional/MathClass.php";
	
	class Polynom{
		
		public static function find($arr,$x){
			$result = 0;
			$xPower = 1;
			
			for($i = 0; $i < count($arr); $i++){
				$result += $arr[$i]*$xPower;
				$xPower *= $x;
			}
			
			return $result;
		}
		
		public static function findDiff($arr,$x){
			$result = $arr[0];
			
			for($i = 1; $i < count($arr); $i++){
				$result += $arr[$i]*$i*MathClass::power($x,$i-1);
			}
			
			return $result;
		}
		
		
	}
	
	$arr = array(2,-5,1,7);
	
	print_r($arr);
	echo Polynom::find($arr,2)." ";

	echo Polynom::findDiff($arr,2)." ";	