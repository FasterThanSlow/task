<?php

	include "additional/DelInsSw.php";
	include "additional/Sorter.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned(){
			$arrPositive = $this->cutNumsPositive($this->n);
			$arrNegative = $this->cutNumsNegative($this->n);
			$arrPositive = Sorter::sortD($arrPositive);
			$arrNegative = Sorter::sortU($arrNegative);
			$result = DIS::concat($arrNegative,$arrPositive);
			return $result;
		}
	
		private function cutNumsPositive($arr){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] >= 0){
					$result[] = $arr[$i];
				}
			}
			return $result;
		}
		
		private function cutNumsNegative($arr){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] < 0){
					$result[] = $arr[$i];
				}
			}
			return $result;
		}
	}
	$arr = DIS::getRandArr(10);
	//$arr = array(-1,4,0,4,19,6,-8,0,6,0,1,0,0);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned());