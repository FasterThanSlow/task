<?php
	include "additional/Sorter.php";
	include "additional/DelInsSw.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned($k){
			$result = Sorter::sortU($this->n);
			$flag = true;
			$arr = $k;
	
			
			for($i = 0; $i < count($k); $i++){
				for($j = 0; $j < count($result); $j++){
					if($result[$j] >= $k[$i]){
						$result = DIS::insertElement($result,$j,$k[$i]);
						$flag = false;
						break;
					}
				}
				if($flag){
					$result = DIS::insertElement($result,count($result),$k[$i]);	
				}
				$flag = true;
				
			}
			
			return $result;
		}
	}
	
	
	//$arr = DIS::getRandArr(10);
	$arr = array(1,7,9,3,6,5);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned(array(10,2,8)));