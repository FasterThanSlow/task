<?php

	include "additional/DelInsSw.php";
	include "additional/Sorter.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned(){
			$arr0 = $this->cutNum($this->n,0);
			$arrWithout0 = $this->cutNumWithout($this->n,0);
			$arrWithout0 = Sorter::sortU($arrWithout0);
			$result = DIS::concat($arr0,$arrWithout0);
			return $result;
		}
	
		private function cutNumWithout($arr,$n){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] != $n){
					$result[] = $arr[$i];
				}
			}
			return $result;
		}
		
		private function cutNum($arr,$n){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] == $n){
					$result[] = $n;
				}
			}
			return $result;
		}
	}
	//$arr = DIS::getRandArr(10);
	$arr = array(-1,4,0,4,19,6,-8,0,6,0,1,0,0);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned());