<?php

	include "functions.php";
	include "MaxIMin.php";
	
	class Finder extends MaxIMin{
		
		public function __construct($n){
			parent::__construct($n);
		}
		
		public function find2Max(){
			$first = $this->findMaxValue();
			$firstIndex = $this->findMaxIndex();
			$second = isset($this->n[$this->findMaxIndex() + 1]) ? $this->n[$this->findMaxIndex() + 1] : $this->n[$this->findMaxIndex() - 1];
			
			for($i = $this->findMaxIndex() + 1; $i < count($this->n); $i++){
				if($this->n[$i] > $second){
					$second = $this->n[$i];
				}
			}
			
			for($i = $this->findMaxIndex() - 1; $i > 0; $i--){
				if($this->n[$i] > $second){
					$second = $this->n[$i];
				}
			}
			
			return array($first,$second);
		}
		
		public function find2Min(){
			$first = $this->findMinValue();
			$firstIndex = $this->findMinIndex();
			$second = isset($this->n[$this->findMinIndex() + 1]) ? $this->n[$this->findMinIndex() + 1] : $this->n[$this->findMinIndex() - 1];
			
			for($i = $this->findMinIndex() + 1; $i < count($this->n); $i++){
				if($this->n[$i] < $second){
					$second = $this->n[$i];
				}
			}
			
			for($i = $this->findMinIndex() - 1; $i > 0; $i--){
				if($this->n[$i] < $second){
					$second = $this->n[$i];
				}
			}
			
			return array($first,$second);
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new Finder($arr);
	printArr($finder->find2Max());
	printArr($finder->find2Min());