<?php

	include "functions.php";

	function revertNegAndPos($n){
		if(is_array($n)){
			$result = $n;
			$firstPositivePos = 0;
			$fPos = 0;
			$lNeg = 0;
			$lastNegativePos = 0;
			$flag = true;
			
			for($i = 0; $i < count($result); $i++){
				if($result[$i] > 0 && $flag){
					$firstPositivePos = $i;
					$fPos = $result[$i];
					$flag = false;
				}
				if($result[$i] < 0){
					$lastNegativePos = $i;
					$lNeg = $result[$i];
				}
			}
			
			for($i = 0; $i < count($result); $i++){
				if($i == $firstPositivePos){
					$result[$i] = $lNeg;
				} 
				if($i == $lastNegativePos){
					$result[$i] = $fPos;
				}
			}
			
			return $result;
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	echo "<br>";
	printArr(revertNegAndPos($arr));