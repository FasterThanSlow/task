
		
		<?php

	include "additional/DelInsSw.php";
	include "additional/Sorter.php";
	
	class Positioner{
		private $a,$b;
		
		public function __construct($a,$b){
			$this->a = $a;
			$this->b = $b;
		}
		
		public function positioned(){
			$arr1 = $this->a;
			$arr2 = $this->b;
			$k = DIS::concat($arr1,$arr2);
			$k = Sorter::sortU($k);
			return $k;
		}
	}
	$arr1 = DIS::getRandArr(5);
	$arr2 = DIS::getRandArr(5);
	//$arr = array(-1,4,0,4,19,6,-8,0,6,0,1,0,0);
	DIS::printArr($arr1);
	DIS::printArr($arr2);
	$poser = new Positioner($arr1,$arr2);
	DIS::printArr($poser->positioned());