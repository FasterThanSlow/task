<?php
	
	include "additional/MaxAndMin.php";
	include "additional/DelInsSw.php";
	
	class Deleter{
		
		public static function deleting($arr,$k){
			$result = $arr;
			$j=0;
			$maxIndex = MaxAndMin::findMaxIndex($result);
			for($i = $maxIndex + 1; $i <= $maxIndex + $k; $i++){
				$result = DIS::deleteElement($result,$i-$j);
				$j++;
			}
			return $result;
		}
		
	}
	
	$arr = array(1,5,-1,9,2,4,6,8,1,-1,2);
	echo "<pre>".print_r($arr,true)."</pre>";
	echo "<pre>".print_r(Deleter::deleting($arr,5),true)."</pre>";