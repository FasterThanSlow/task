<?php
	
	include "functions.php";
	
	class MaxIMin{
		
		protected $n;
		
		public function __construct($n){
			if(is_array($n))
				$this->n = $n;
		}
		
		public function findMaxIndex(){
			$max = $this->n[0];
			$index = 0;
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] > $max){
					$max = $this->n[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
		public function findMaxValue(){
			$max = $this->n[0];
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] > $max){
					$max = $this->n[$i];
				}
			}
			return $max;
		}
		
		public function findMinIndex(){
			$min = $this->n[0];
			$index = 0;
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] < $min){
					$min  = $this->n[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
		public function findMinValue(){
			$min  = $this->n[0];
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] < $min){
					$min  = $this->n[$i];
				}
			}
			return $min ;
		}
		
	}
	
	
	class ChangerMAM extends MaxIMin{
		
		public function __construct($n){
			parent::__construct($n);
		}
		
		
		public function change(){
			$result = $this->n;
			$max = $this->findMaxValue();
			$min = $this->findMinValue();
			
			for($i = 0; $i < count($result); $i++){
				if($result[$i] % 2 == 0){
					$result[$i] = $max;
				}
				if($result[$i] % 2 != 0){
					$result[$i] = $min;
				}
			}
			return $result;
		}
		
		
	}
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new ChangerMAM($arr);
	printArr($finder->change());
	