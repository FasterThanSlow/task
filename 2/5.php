<?php

	include "functions.php";

	function changeMinAndMax($n){
		if(is_array($n)){
			$min = $n[0];
			$maxPos = 0;
			$max = $n[0];
			$minPos = 0;
			
			for($i = 1; $i < count($n); $i++){
				if($min > $n[$i]){
					$min = $n[$i];
					$minPos = $i;
				}
				if($max < $n[$i]){
					$max = $n[$i];
					$maxPos = $i;
				}
			}
			
			$result = $n;
			
			for($i = 0; $i < count($result); $i++){
				if($i ==  $maxPos){
					$result[$i] = $min;
				}
				if($i == $minPos){
					$result[$i] = $max;
				}
			}
			return $result;
		}
		
	}
	
	
	$arr = randArr(10);
	printArr($arr);
	echo "<br>";
	printArr(changeMinAndMax($arr));