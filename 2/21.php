<?php
include "functions.php";
class Selecter{
	private $n;
	
	public function __construct($n){
		$this->n = $n;
	}
	
	public function select(){
		$count = 0;
		$result = array();
		for($i = 0; $i < count($this->n); $i++){
			for($j = 0; $j < count($this->n); $j++){
				if($this->n[$j] == $this->n[$i]){
					$count++;
				}	
			}
			if($count >= 2 && !self::isexist($result,$this->n[$i])){
				$result[] = $this->n[$i];
			}
			$count=0;
		}
		return $result;	
	}
	
	public static function isexist($arr,$p){
		for($i = 0; $i < count($arr); $i++){
			if($arr[$i] == $p){
				return true;
			}
		}
		return false;
	}
	
}

	//$arr = array(1,2,4,3,5,3,9,1);
	$arr = randArr(10);
	printArr($arr);
	
	$counter = new  Selecter($arr);
	printArr($counter->select());