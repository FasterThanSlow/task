<?php

	include "MaxIMin.php";
	include "functions.php";
	
	class Inserter extends MaxIMin{
		
		public function __construct($n){
			parent::__construct($n);
		}
		
		public function insert(){
			$result = $this->n;
			$max = $this->findMaxValue();
			for($i = 0; $i < count($result); $i++){
				if($result[$i] < 0 && $result[$i] % 2 == 0){
					$result = $this->insertAfter($result,$i,$max);
				}
			}
			return $result;
		}
		
		public static function insertAfter($arr, $index, $element){
			$result = $arr;
			$result[] = $result[count($result)-1];
			for($i = count($result)-1; $i > $index; $i--){
				$result[$i] = $result[$i-1];
			}
			$result[$index+1] = $element;
			return $result;
		}
	}
	
	$arr = array(1,8,-7,-6,12,-16,19,90,-150);
	//$arr = randArr(10);
	printArr($arr);
	//$arr = array(1,0,0,0,0,9,0,0,0,0,0,0,3,5,4);
	$searcher = new Inserter($arr);
	printArr($searcher->insert());