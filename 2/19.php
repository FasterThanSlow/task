<?php
	include "functions.php";
	class Changer{
		private $n;
		
		public function __construct($n){
			if(is_array($n)){
				$this->n = $n;
			}
		}
		
		public function change($p){
			$result = $this->n;
			$elementIndex = $this->search($p);
			for($i = 0; $i < count($result); $i++){
				if($i == $elementIndex){
					for($j = $elementIndex; $j < count($result)-1; $j++){
						$result[$j] = $result[$j + 1];
					}
					$result[count($result)-1] = $elementIndex;
				}
			}
			return $result;
		}
		
		private function search($p){
			$elementIndex = 0;
			
			for($i = 0; $i < count($this->n); $i++){
				if($this->n[$i] > 0 && $this->n[$i] % 2 == 0 && $this->n[$i] % $p == 0){
					$elementIndex = $i;
				}
			}
			
			return $elementIndex;
		}
	}
	
	$arr = array(1,2,4,3,5,3,9,1);
	//$arr = randArr(10);
	printArr($arr);
	
	$counter = new Changer($arr);
	printArr($counter->change(2));