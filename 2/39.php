<?php

	include "additional/DelInsSw.php";
	include "additional/Sorter.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned(){
			$nums = array();
			$counts = $this->cutNums($this->n,$nums);
			
			$result = array();
			$counts = $this->sortU($counts,$nums);
			
			for($i = 0; $i < count($nums); $i++){
				for($j = 0; $j < $counts[$i]; $j++){
					$result[] = $nums[$i];
				}
			}
			return $result;
		}
	 
		public static function sortU($arr,&$arr1){
			$result = $arr;
			for ($i = count($result) - 1; $i >= 0; $i--) {
				for ($j = 0; $j <= ($i-1); $j++){
					if ($result[$j] > $result[$j+1]) {
						
						$temp = $arr1[$j];
						$arr1[$j] = $arr1[$j+1];
						$arr1[$j+1] = $temp;
						
						$temp = $result[$j];
						$result[$j] = $result[$j+1];
						$result[$j+1] = $temp;
					}
				}
			}
			return $result;
        }
		
		private function cutNums($arr,&$k = null){
			$result = array();
			$count = 0;
			
			for($i = 0; $i <= 20; $i++){
				for($j = 0; $j < count($arr); $j++){
					if($i == $arr[$j]){
						$count++;
					}
				}
				if($count>0){
					$k[] = $i;
					$result[] = $count;
				}
				$count = 0;
			}
			return $result;
		}
	}
	$arr = DIS::getRandArr(20);
	//$arr = array(-1,4,0,4,19,6,-8,0,6,0,1,0,0);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned());