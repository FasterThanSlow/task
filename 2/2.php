<?php
	include "functions.php";
	
	function arrByAvarage($n){
		if(is_array($n)){
			$result = array();
			$summ = 0;
			for($i = 1;$i <= count($n); $i++){
				for($j = 0; $j < $i; $j++){
					$summ += $n[$j];
				}
				$result[] = $summ;
				$summ = 0;
			}
			return $result;
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	echo "<br>";
	printArr(arrByAvarage($arr));