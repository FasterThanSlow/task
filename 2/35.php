<?php
	include "additional/DelInsSw.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned(){
			$arr0 = $this->cutNum($this->n,0);
			$arr1 = $this->cutNum($this->n,1);
			$arr2 = $this->cutNum($this->n,2);
			$result = DIS::concat($arr0,$arr2);
			$result = DIS::concat($result,$arr1);
			return $result;
		}
	
		private function cutNum($arr,$n){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] == $n){
					$result[] = $n;
				}
			}
			return $result;
		}
	}
	
	$arr = array(1,0,1,2,0,1,1,0,2,2,0,1,2);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned());