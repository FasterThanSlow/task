<?php

	include "functions.php";
	
	class Counter{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function counting(){
			$count = 0;
			$counting = true;
			
			for($i = 0; $i < count($this->n); $i++){
				for($j = $i+1; $j < count($this->n); $j++){
					if($this->n[$i] == $this->n[$j]){
						$counting = false;
					}
				}
				if($counting){
					$count++;
				}
				$counting = true;
			}
			return $count;
		}
	}
	//$arr = array(1,2,5,7,5,2,9,1);
	$arr = randArr(10);
	printArr($arr);
	
	$counter = new Counter($arr);
	echo $counter->counting();