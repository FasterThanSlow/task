<?php

	include "functions.php";
	
	function posFirstNegative($n){
		if(is_array($n)){
			$count=0;
			for($i = 0; $i < count($n); $i++){
				$count++;
				if($n[$i] < 0)
					return $count-1;
			}
		}
		return null;
	}
	
	
	function summOddAfterLastNegative($n){
		if(is_array($n)){
			$posLastNegative = 0;
			for($i = 0; $i < count($n); $i++){
				if($n[$i] < 0){
					$posLastNegative = $i;
				}
			}
			$summ = 0;
			for($i = $posLastNegative + 1; $i < count($n); $i++){
				if($n[$i] % 2 != 0){
					$summ += $n[$i];
				}
			}
			return $summ;
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	echo "<br>";
	echo posFirstNegative($arr);
	
	
	
	printArr($arr);
	echo "<br>";
	echo summOddAfterLastNegative($arr);