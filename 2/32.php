<?php

	include "additional/Sorter.php";
	include "additional/DelInsSw.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned(){
			$result = $this->n;
			$oddArr = $this->getSortedOddArr($this->n);
			$evenArr = $this->getSortedEvenArr($this->n);
			$posOdd = 0;
			$posEven = 0;
			
			for($i = 0; $i < count($result); $i++){
				if($result[$i] % 2 == 0){
					$result[$i] = $evenArr[$posEven];
					$posEven++; 
				}
				else{
					$result[$i] = $oddArr[$posOdd];
					$posOdd++;
				}
			}
			
			return $result;
		}
		
		private function getSortedOddArr($arr){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] % 2 != 0){
					$result[] = $arr[$i];
				}
			}
			return Sorter::sortD($result);
		}
		
		
		
		private function getSortedEvenArr($arr){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i] % 2 == 0){
					$result[] = $arr[$i];
				}
			}
			return Sorter::sortU($result);
		}
	}
	$arr = array(1,8,2,3,4,6,7);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned());