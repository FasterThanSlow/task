<?php

include "functions.php";

class Delimeter{	 
	private $n;
	
	public function __construct($n){
		$this->n = $n;
	}
	
	public function Negative(){
		$result = array();
		for($i = 0; $i < count($this->n); $i++){
			if($this->n[$i] < 0){
				$result[] = $this->n[$i]; 
			}
		}
		return $result;
	}
	
	
	public function Positive(){
		$result = array();
		for($i = 0; $i < count($this->n); $i++){
			if($this->n[$i] > 0){
				$result[] = $this->n[$i]; 
			}
		}
		return $result;
	}
	
	
	public function Event(){
		$result = array();
		for($i = 0; $i < count($this->n); $i++){
			if($this->n[$i] % 2 == 0){
				$result[] = $this->n[$i]; 
			}
		}
		return $result;
	}
	
	
	public function NotEvent(){
		$result = array();
		for($i = 0; $i < count($this->n); $i++){
			if($this->n[$i] % 2 != 0){
				$result[] = $this->n[$i]; 
			}
		}
		return $result;
	}
 }
 
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new Delimeter($arr);
	printArr($finder->Negative());
	printArr($finder->Positive());
	printArr($finder->Event());
	printArr($finder->NotEvent());
	
	