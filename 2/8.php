<?php 

	include "functions.php";
	
	class Changer{
		private $n;
		
		public function __construct($n){
			if(is_array($n)){
				$this->n = $n;
			}
		}
		
		public function findIndexLastOdd(){
			$index = 0;
			for($i = 0; $i < count($this->n); $i++){
				if($this->n[$i] < 0){
					$index = $i;
				}
			}
			return $index;
		}
		
		public function findValueLastOdd(){
			$value = 0;
			for($i = 0; $i < count($this->n); $i++){
				if($this->n[$i] < 0){
					$value = $this->n[$i];
				}
			}
			if($value != 0)
				return $value;
			else
				return false;
		}
		
		public function findMaxIndex(){
			$max = $this->n[0];
			$index = 0;
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] > $max){
					$max = $this->n[$i];
					$index = $i;
				}
			}
			return $index;
		}
		
		
		public function findMaxValue(){
			$max = $this->n[0];
			
			for($i = 1; $i < count($this->n); $i++){
				if($this->n[$i] > $max){
					$max = $this->n[$i];
				}
			}
			return $max;
		}
		
		
		public function chOddWithMax(){
			$result = $this->n;
			for($i = 0; $i < count($result); $i++){
				if($this->findIndexLastOdd()){
					if($i == $this->findIndexLastOdd()){
						$result[$i] = $this->findMaxValue();
					}
					if($i == $this->findMaxIndex()){
						$result[$i] = $this->findValueLastOdd();
					}
				}
			}
			return $result;
		}
	}
	
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new Changer($arr);
	printArr($finder->chOddWithMax());

	