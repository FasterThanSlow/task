<?php
	include "functions.php";
	class SearcherSequences{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function search(){
			$count = 0;
			$maxCount = 0;
			$iBegin = 0;
			$iEnd = 0;
			
			for($i = 0; $i < count($this->n)-1; $i++){
				if($this->n[$i] == 0){
					if($count==0){
						$buff = $i;
					}
					$count++;
				}else{
					$count=0;
					
				}
				if($maxCount < $count){
					$iBegin = $buff;
					$maxCount = $count;
					$iEnd = $iBegin + $maxCount - 1;
				}
			}
			return array($iBegin,$iEnd);
		}
	}

	$arr = array(1,0,0,0,0,9,0,0,0,0,0,0,3,5,4);
	$searcher = new SearcherSequences($arr);
	printArr($searcher->search());