<?php

	class DIS{
		public static function deleteElement($arr,$index){
			$result = $arr;
			for($i = $index; $i < count($result)-1; $i++){
				$result[$i] = $result[$i+1];
			}
			unset($result[count($result)-1]);
			return $result;
		}
		
		public static function insertElement($arr,$index,$element){
			$result = $arr;
			$result[] = $result[count($result)-1];
			for($i = count($result) - 1; $i > $index; $i--){
				$result[$i] = $result[$i-1];
			}
			$result[$index] = $element;
			return $result;
		}
		
		public static function cutArray($arr,$iBegin,$iEnd){
			$result = array();
			for($i= $iBegin; $i <= $iEnd; $i++){
				$result[] = $arr[$i];
			}
			return $result;
		}
		
		public static function getRandArr($n){
			$result = array();
			for($i = 0; $i < $n; $i++){
				$result[] = rand(0,20);
			}
			return $result;
		}
	
		public static function printArr($arr){
			echo "<pre>".print_r($arr,true)."</pre>";
		}
		
		public static function getSumm($arr){
			$result = 0;
			for($i = 0; $i < count($arr); $i++){
				$result += $arr[$i];
			}
			return $result;
		}
		
		public static function concat($arr1,$arr2){
			for($i = 0; $i < count($arr2); $i++){
				$arr1[] = $arr2[$i];
			}
			return $arr1;
		}
		
	}