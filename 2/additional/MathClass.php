<?php

	class MathClass{
		
		public static function power($n,$power){
			$result = 1;
			
			for($i = 1; $i <= $power; $i++){
				$result *= $n;
			}
			
			return $result;
		}
	
		
	}