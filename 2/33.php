<?php
	include "additional/Sorter.php";
	include "additional/DelInsSw.php";
	
	class Positioner{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function positioned($p){
			$result = $this->n;
			$positiveArr = $this->getSortPositive($result);
			$positiveCount = 0;
			
			for($i = 0; $i < count($result); $i++){
				if($result[$i] > 0){
					$result[$i] = $positiveArr[$positiveCount];
					$positiveCount++;
				}
			}
			$result = $this->insertSort($result,$p);
			return $result;
		}
		
		private function insertSort($arr,$p){
			$result = $arr;
			for($i = 0; $i < count($result);$i++){
				if($result[$i] > 0 && $result[$i] > $p){
					$result = DIS::insertElement($result,$i,$p);
					return $result;
				}
			}
		}
		
		private function getSortPositive($arr){
			$result = array();
			for($i = 0; $i < count($arr); $i++){
				if($arr[$i]  > 0){
					$result[] = $arr[$i];
				}
			}
			return Sorter::sortU($result); 
		}
		
	}
	
	$arr = DIS::getRandArr(10);
	DIS::printArr($arr);
	$poser = new Positioner($arr);
	DIS::printArr($poser->positioned(10));