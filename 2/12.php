<?php

	include "functions.php";
	
	class Finder{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		}
		
		public function findMaxNeg(){
			$negIndex = $this->findFirstNeg();
			if(isset($negIndex)){
				$max = $this->n[$negIndex];
				for($i = $negIndex; $i < count($this->n); $i++){
					if($this->n[$i] > $max && $this->n[$i] < 0){
						$max = $this->n[$i];
					}
				}
				return $max;
			}
			else
				return false;
		}
		
		
		
		public function findMinPos(){
			$posIndex = $this->findFirstPos();
			if(isset($posIndex)){
				$min = $this->n[$posIndex];
				for($i = $posIndex; $i < count($this->n); $i++){
					if($this->n[$i] < $min && $this->n[$i] > 0){
						$min = $this->n[$i];
					}
				}
				return $min;
			}
			else
				return false;
		}
		
		public function findFirstNeg(){
			for($i = 0; $i < count($this->n); $i++){
				if($this->n[$i] < 0){
					return $i;
				}
			}
			return null;
		}
		
		
		public function findFirstPos(){
			for($i = 0; $i < count($this->n); $i++){
				if($this->n[$i] > 0){
					return $i;
				}
			}
			return null;
		}
	}
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new Finder($arr);
	echo  $finder->findMaxNeg()."<br>";
	echo  $finder->findMinPos()."<br>";