<?php

	include "functions.php";

	class FinderSumms{
		private $n;
		
		public function __construct($n){
			if(is_array($n)){
				$this->n = $n;
			}
		}
		
		public function findMaxSumm(){
			$maxSumm = $this->n[0] + $this->n[1];
			$index = 0;
			for($i = 1; $i < count($this->n) - 1; $i++){
				$currSumm = $this->n[$i] + $this->n[$i+1];
				if($maxSumm < $currSumm){
					$maxSumm = $currSumm;
					$index = $i;
				}
			}
			return $index;
		}
		
		public function findMinSumm(){
			$minSumm = $this->n[0] + $this->n[1];
			$index = 0;
			for($i = 1; $i < count($this->n) - 1; $i++){
				$currSumm = $this->n[$i] + $this->n[$i+1];
				if($minSumm > $currSumm){
					$minSumm = $currSumm;
					$index = $i + 1;
				}
			}
			return $index;
		}
		
	}
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new FinderSumms($arr);
	echo $finder->findMaxSumm()."<br>";
	echo $finder->findMinSumm()."<br>";
	