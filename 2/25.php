<?php

	include "MaxIMin.php";
	include "functions.php";
	
	class Inserter extends MaxIMin{
		
		public function __construct($n){
			parent::__construct($n);
		}
		
		public function insert($k){
			$result = $this->n;
			$min = $this->findMinValue();
			for($i = $this->findMaxIndex(); $i <= $this->findMaxIndex() + $k - 1; $i++){
				$result[$i] = $min;
			}
			return $result;
		}
	}
	$arr = randArr(10);
	printArr($arr);
	//$arr = array(1,0,0,0,0,9,0,0,0,0,0,0,3,5,4);
	$searcher = new Inserter($arr);
	printArr($searcher->insert(4));