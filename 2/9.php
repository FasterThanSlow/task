<?php

	include "functions.php";
	
	class Finder{
		private $n;
		
		public function __construct($n){
			$this->n = $n;
		} 
		
		public function findFirstEven(){
			for($i = 0; $i< count($this->n); $i++){
				if($this->n[$i] % 2 == 0){
					return $i;
				}
			}
		}
		
		
		public function findFirstNotEven(){
			for($i = 0; $i< count($this->n); $i++){
				if($this->n[$i] % 2 != 0){
					return $i;
				}
			}
		}
		
		public function findMaxEven(){
			$maxEven = $this->n[$this->findFirstEven()];
			
			for($i = $this->findFirstEven(); $i < count($this->n); $i++){
				if($this->n[$i] > $maxEven){
					if($this->n[$i] % 2 == 0){
						$maxEven = $this->n[$i];
					}
				}
			}
			
			return $maxEven;
		}
		
		public function findMinNotEven(){
			$minNotEven = $this->n[$this->findFirstNotEven()];
			
			for($i = $this->findFirstNotEven(); $i < count($this->n); $i++){
				if($this->n[$i] < $minNotEven){
					if($this->n[$i] % 2 != 0){
						$minNotEven = $this->n[$i];
					}
				}
			}
			
			return $minNotEven;
		}
		
		
	}
	
	$arr = randArr(10);
	printArr($arr);
	
	$finder = new Finder($arr);
	echo $finder->findMaxEven()."<br>";
	echo $finder->findMinNotEven()."<br>";
	
	