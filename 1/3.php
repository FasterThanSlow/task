<?php

	$n = 128589;
	$count = 0;
	$buf = 0;
	$k = 9;
	$result = true;
	
	while($result && $n >= 1){
		$buf = $k;
		$k = $n % 10;
		if($buf < $k){
			$result = false;
		}
		$n /= 10;
	}
	
	if($result) 
		echo "Являеться возрастающей последовательностью";
	else 
		echo"Не является возрастающей последовательностью";