<?php

	function isPerfect($n){
		$summ = 0;	
		for($i = 1; $i < $n; $i++){
			if($n % $i == 0){
				$summ += $i;
			}
		}
		if($summ == $n)
			return true;
		else 
			return false;
	}
	
	
	function perfectDiap($n,$m){
		$result = array();
		for($i = $n; $i <= $m ; $i++){
			if(isPerfect($i))
				$result[] = $i;
		}
		return $result;
	}
	
	print_r(perfectDiap(1,1000));