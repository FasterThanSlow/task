<?php

	include "functions.php";
	
	function findMaxDevSumm($n,$m){
		$summ = summSimpleDel($n);
		for($i = $n+1; $i <= $m; $i++){
			$newSumm = summSimpleDel($i);
			if($summ < $newSumm){
				$summ = $newSumm; 
			}
		}
		return $summ;
	}
	
	function summSimpleDel($n){
		$summ = 0;
		for($i = 1; $i < $n; $i++){
			if($n % $i == 0){
				if(isSimple($i))
					$summ += $i;
			}
		}
		return $summ;
	}
	
	echo findMaxDevSumm(1,1000);