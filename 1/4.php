<?php
	$countUpper = 0;
	$countDown = 0;
	$result = array();
	$number = 0;
	
	for($i = 1000; $i < 9999; $i += 2){
		$buffi = $i;
		
		while($buffi >= 1){
			$buffNum = $number;
			$number = $buffi % 10;
			
			if($buffNum < $number)
				$countUpper++;
			elseif($buffNum != $number)
				$countDown++;
			
			$buffi /= 10;
		}
		
		if($countDown == 4 || $countUpper == 4){
			$result[] = $i;
		}
		
		$countDown = 0;
		$countUpper = 0;
	}
	
	print_r($result);