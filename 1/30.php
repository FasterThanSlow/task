<?php
	include "functions.php";
	
	function findTwins($n){
		$result = array();
		for($i = $n; $i < 2*$n - 1; $i++){
			for($j = $n; $j < 2*$n - 1; $j++){
				if(isSimple($i) && isSimple($j)){
					if($i > $j){
						if($i - $j == 2 || $j - $i == 2){
							$result[] = "$i и $j близнецы";
						}
					}
				}
			}
		}
		return $result;
	}
	
	echo "<pre>".print_r(findTwins(100),true)."</pre>";