<?php

	include "functions.php";
	
	function createMaxNumber($n,$m){
		$retValue = 0;
		$result = array();
		$max = 0;
		
		for($i = $n; $i <= $m; $i++){
			$temp = $i;
			$r = findPosition($temp,0);
				
			while($temp >= 1){
				$max = findMax($temp);
				$retValue += $max;
				$temp = deleteByPosition($temp,findPosition($temp,$max));
				if($temp >= 1){
					$retValue *= 10;
				}
			}
			if(isset($r))
				$retValue *= 10;
			$result[] = $retValue;
			$retValue = 0;
		}
		
		return $result;
	}
	
	function findMax($n){
		$max = $n % 10;
		while($n >= 1){
			$k = $n % 10;
			if($k > $max){
				$max = $k;
			}
			$n /= 10;
		}
		return $max;
	}
	
	
	echo "<pre>".print_r(createMaxNumber(122,145),true)."</pre>";