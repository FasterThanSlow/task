<?php

	function summDel($n){
		$summ = 0;
		for($i = 1; $i < $n; $i++){
			if($n % $i == 0){
				$summ += $i;
			}
		}
		return $summ;
	}
	
	function maxSummDel($n,$m){
		$summ = summDel($n);
		for($i = $n+1; $i <= $m; $i++){
			if($summ < summDel($i)){
				$summ = summDel($i); 
			}
		}
		return $summ;
	}
	
	
	echo maxSummDel(1,1000);