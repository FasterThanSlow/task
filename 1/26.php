<?php
		
	function findMaxDevSumm($n,$m){
		$summ = summDel($n);
		for($i = $n+1; $i <= $m; $i++){
			$newSumm = summDel($i);
			if($summ < $newSumm){
				$summ = $newSumm; 
			}
		}
		return $summ;
	}
	
	function summDel($n){
		$summ = 0;
		for($i = 1; $i < $n; $i++){
			if($n % $i == 0){
				$summ += $i;
			}
		}
		return $summ;
	}
	
	echo findMaxDevSumm(1,1000);
	