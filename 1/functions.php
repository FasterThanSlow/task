<?php

	function repeat($n){
		$number = 0;
		$temp = 0;
		$buff = 0;
		$result = false;
		
		while ($n >= 1){
			$number = $n % 10;
			$n /= 10;
			$temp = $n;
			
			while($temp >= 1){
				$buff = $temp % 10;
				if($buff == $number)
				$result = true;
				$temp /= 10;
			}			
		}
		return $result;
	}
	
	function revert($n){
		$result = 0;
		while($n >= 1){
			$result += $n % 10;
			$n /= 10;
			if($n >= 1)
				$result *= 10;
		}
		return $result;
	}
	
	function summNumber($n){
		if($n < 0){
			$n = -$n;
		}
		$result = 0;
		while($n != 0){
			$result += $n % 10;
			$n /= 10;
		}
		return $result;
	}
	
	function isSimple($n){
		if($n < 0){
			$n = -$n;
		}
		$result = true;
		for($i = 2; $i < $n; $i++){
			if($n % $i == 0){
				$result = false;
				break;
			}
		}
		return $result;
	}
	
	function numCount($n){
		$count = 0;
		while($n >= 1){
			$count++;
			$n /= 10;
		}
		return $count;
	}
	
	function findPosition($n,$i){
		$position = null;
		$count = 0;
		while($n >= 1){
			$k = $n % 10;
			$count++;
			if($k == $i){
				$position = $count;
			}
			$n /= 10;
		}
		return $position;
	}
	
	function findNumByPos($n,$pos){
		$count = 0;
		while($n >= 1){
			$k = $n % 10;
			$count++;
			if($count == $pos){
				return $k;
			}
			$n /= 10;
		}
	}
	
	function deleteByPosition($n,$i){
		$count = 1;
		$temp = $n;
		$result = 0;
		
		while($temp >= 1){
			if($count != $i){
				$result += $temp % 10;
				$temp /= 10;
				if($temp >= 1){
					$result *= 10;
				}
			}
			else
				$temp /= 10;
			$count++;
			
		}
		
		return revert($result);
	}