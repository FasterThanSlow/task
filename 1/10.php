<?php
	include "functions.php";
	
	function isPolindrom($n){
		if(revert($n) == $n){
			return true;
		}
		else
			return false;
	}
	
	function sqPolindrom($n){
		$result = array();
		
		for($i = $n; $i > 0; $i--){
			if(isPolindrom($i*$i)){
				$result[] = $i;
			}
		}
		return $result;
	}
	
	print_r(sqPolindrom(200));