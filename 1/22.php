<?php

	include "functions.php";
	
	function simpleDev($n){
		$flag = true;
		$result = array();
		
		while($flag){
			if($n == 1){
				$flag = false;
			}
			else{
				for($i = 1; $i <= $n; $i++){
					if($i > 1 && isSimple($i)){
						if($n % $i == 0){
							$n /= $i;
							$result[] = $i;
							break;
						}
					}
				}
			}
		}
		
		return $result;
	}
	
	print_r(simpleDev(48));
	