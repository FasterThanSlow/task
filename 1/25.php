<?php

	function fact($n){
		if($n == 1)
			return 1;
		else
			return $n * fact($n-1);
	}
	
	function find($n,$m){
		$count = 0;
		$summ = 0;
		for($i = $n; $i < $m; $i++){
			if(summFact($i) == $i){
				$summ+=$i;
				$count++;
			}
		}
		echo "Диапозон $n до $m<br>";
		echo "Сумма чисел равных сумме факториалов своих цифр = $summ<br>";
		echo "Количество: $count<br>";
		return $count;
	}
	
	function summFact($n){
		$summ = 0;
		while($n >= 1){
			$k = $n % 10;
			if($k > 0){
				$summ += fact($k);
			}
			$n /= 10;
		}
		return $summ;
	}
	
	find(1,1000);