<?php
	include "functions.php";
	
	
	function twoSimple($a,$b){
		$result = false;
		for($i = 1; $i <= $a; $i++){
			$result = false;
			if($a % $i == 0){
					if($b % $i != 0)
						$result = true;
					elseif($i > 1)
						break;
			}
			for($j = 1; $j <= $b; $j++){
				if($b % $j == 0){
					if($a % $j != 0)
						$result = true;
					elseif($j>1) 
						break;
				}
			}
			
		}
		return $result;
	}
	
	if(twoSimple(23,9)) echo "Числа взаимно простые";
	else echo "Числа не являются взаимно простыми";