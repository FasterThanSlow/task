<?php
	
	include "functions.php";
	
	function convertToBin($n) {
		$n = abs($n);
		$result = '';
		while ($n != 0) {
			$result = ($n % 2) . $result;
			$n = (int)( $n / 2 );
		}
		if($result == 1){
			$result = "0".$result;
		}
		return $result;
	}
	
	echo convertToBin(1);