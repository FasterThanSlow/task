<?php

function nok($n, $m){
	if($n > $m)
		$maximum = $n;
	else 
		$maximum = $m;
	
    for($i = $maximum; $i > 0; $i++){
        if(($i % $n == 0) && ($i % $m == 0)){
            return $i;
        }
    }
}

echo nok(5,16);