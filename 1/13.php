<?php
	
	
	function isSimmetric($n,$m){
		$result = false;
		$number = pow(2,$n) + $m;
		$temp = 0;
		$firstHalf = 0;
		$k = 0;
		
		if(numCount($number) % 2 == 0){
			$secondHalf = $number % (pow(10,numCount($number)/2));
			$firstHalf = (int) ($number / (pow(10,numCount($number)/2)));
			
			if($secondHalf == $firstHalf){
			$result = true;
			}
		}
		
		return $result;
	}
	
	function numCount($n){
		$count = 0;
		while($n>=1){
			$count++;
			$n /= 10;
		}
		return $count;
	}
	
	if(isSimmetric(11,-28)) echo "Числа симметричны";
	else echo "Числа не симметричны";